#!/usr/bin/env python3

import logging
import subprocess
import sys
from scapy.all import IP,sr1,ARP

logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

if len(sys.argv)<2:
    print("Usage - ./arp_disc.py [interface]")
    print("Example - ./arp_disc.py wlp2s0")
    print("Example will perform an ARP scan of the local subnet to which eth0 is assigned")
    exit()

interface = str(sys.argv[1]) 
ip = subprocess.check_output("ifconfig "+interface + " |grep inet | tr -s ' ' | cut -d ' ' -f3",shell=True).strip()
ip = str(ip,encoding='utf-8').split()[0]    #bytes to string
ar = ip.split('.')
prefix=ar[0] + '.' + ar[1] +'.'+ ar[2] + '.'
for addr in range(0,254):
    answer = sr1(ARP(pdst=prefix+str(addr)),timeout=0.1,verbose=0)
    if answer==None:
        pass
    else:
        print(prefix+str(addr))
